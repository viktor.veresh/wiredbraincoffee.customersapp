﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using WiredBrainCoffee.CustomersApp.Model;

namespace WiredBrainCoffee.CustomersApp.DataProvider
{
    public class CustomerDataProvider : ICustomerDataProvider
    {
        private static readonly string _customerFileName = "customer.json";
        private static readonly StorageFolder _localFolder = ApplicationData.Current.LocalFolder;

        public async Task<IEnumerable<Customer>> LoadCustomerAsync()
        {
            var storageFile = await _localFolder.TryGetItemAsync(_customerFileName) as StorageFile;
            List<Customer> customerList = null;

            if (storageFile == null)
            {
                customerList = new List<Customer>
                {
                    new Customer {FirstName = "Toma", LastName="Huja", IsDeveloper = true},
                    new Customer {FirstName = "Ana", LastName="Roka"},
                    new Customer {FirstName = "Julka", LastName="Master", IsDeveloper = true},
                    new Customer {FirstName = "Uros", LastName="Meric", IsDeveloper = true},
                    new Customer {FirstName = "Sara", LastName="Ranka"},
                    new Customer {FirstName = "Eliza", LastName="Kraljica"},
                    new Customer {FirstName = "Alek", LastName="Biser", IsDeveloper = true},
                };
            }
            else
            {
                using (var stream = await storageFile.OpenAsync(FileAccessMode.Read))
                {
                    using (var dataReader = new DataReader(stream))
                    {
                        await dataReader.LoadAsync((uint)stream.Size);
                        var json = dataReader.ReadString((uint)stream.Size);
                        customerList = JsonConvert.DeserializeObject<List<Customer>>(json);
                    }
                }
            }

            return customerList;
        }

        public async Task SaveCustomerAsync(IEnumerable<Customer> customers)
        {
            var storageFile = await _localFolder.CreateFileAsync(_customerFileName, CreationCollisionOption.ReplaceExisting);

            using(var stream = await storageFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                using(var dataWriter = new DataWriter(stream))
                {
                    var json = JsonConvert.SerializeObject(customers, Formatting.Indented);
                    dataWriter.WriteString(json);
                    await dataWriter.StoreAsync();
                }
            }
        }

    }
}
