WiredBrainCofee.CustomersApp

UWP application.

XAML basics.

Create a layout with Grid.

Use a CustomerDetailControl.

Work with resources and themes.

Apply data binding and MVVM.

Use styles and templates.

Debug XAML application.

Code provided by Pluralsite course XAML: Getting Started by Thomas Claudius Huber.